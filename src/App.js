import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import DataDisplay from './Components/DataDisplay';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          {/*<img src={logo} className="App-logo" alt="logo" />*/}
          <h2>Solution for Coding Problem- FSD201701 - Manish Bana</h2>
        </div>
        <DataDisplay/>
      </div>
    );
  }
}

export default App;
