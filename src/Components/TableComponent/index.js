import React, { Component } from 'react';
import './table.css';

class TableComp extends Component {
  generateHeaders() {
        var cols = this.props.cols;  // [{key, label}]

        // generate our header (th) cell components
        return cols.map(function(colData) {
            return <th key={colData.key}>{colData.label}</th>;
        });
    }
    generateRows() {
       var cols = this.props.cols,  // [{key, label}]
           data = this.props.data;

       return data.map(function(item) {
           // handle the column data within each row
           var cells = cols.map(function(colData) {

               // colData.key might be "firstName"
               return <td>{item[colData.key]}</td>;
           });
           return <tr key={item.id}>{cells}</tr>;
       });
   }
  render() {
       var headerComponents = this.generateHeaders(),
           rowComponents = this.generateRows();

       return (
         <div className="table-wrapper">
         <div className="table-scroll">
           <table className="tabletag">
               <thead className="theadtag">{headerComponents}</thead>
               <tbody className="tbodytag">{rowComponents}</tbody>
           </table>
           </div>
           </div>
       );
   }
}

export default TableComp;
