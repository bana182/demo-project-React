import React, { Component } from 'react';
import axios from 'axios';
//import API from 'fetch-api';
//import Fetch from 'fetch';
import TableComp from '../TableComponent';
import './DataDisplay.css';
class DataDisplay extends Component {
  constructor(props) {
    super(props);

    let cols = [
   { key: 'State_name', label:'State' },
   { key: 'District_Name', label:'District' },
   { key: 'Crop_Year', label:'Crop Year' },
   { key: 'Season', label:'Season' },
   { key: 'Crop', label:'Crop' },
   { key: 'Area', label:'Area' },
   { key: 'Production', label:'Production' }];
    this.state = {
      fieldData: [],
      cols:cols,
      radioChecked:"",
      searchText:"",
      from:'',
      to:'',
      searchBy:"State_name",
      groupBy:"noGroup"
    };
  }
  componentWillMount(){
    axios.get("http://localhost:3000/apy")
       .then(response=>this.setState({fieldData:response.data}));
      // let totalState=[];


  }
  componentDidMount(){
    console.log("fieldData inside componentDidMount",this.state.fieldData);




  }
  groupBySelection(e){
    console.log(e.target.value);
  let value=e.target.value;
    this.setState({
      groupBy:value
    });
  }
containsObject(columnArray,name){
  for(var index=0;index<columnArray.length;index++){
    if(columnArray[index]===name){
      return true;
    }
  }
  return false;
}

generateArray(data,column){
  let total=[];
  for(var index=0;index<data.length;index++){
    if(total.length!==0){
        if(!this.containsObject(total,data[index][column])){
          total.push(data[index][column]);
        }
    }else{
      total.push(data[index][column]);
    }

  }
  return total;
}
groupBy(total,fieldData,groupby){

    let datagroupByState=new Map();

    for(var i=0;i<total.length;i++){
        let SumArea=[];
        let productionSum=[];
        let currentSiteName="";

        for(var j=0;j<fieldData.length;j++){

            if(fieldData[j][groupby]===total[i]){

                if(!datagroupByState.has(fieldData[j][groupby])){
                  datagroupByState.set(fieldData[j][groupby],[]);
                  datagroupByState.get(total[i]).push(fieldData[j]);
                }else{
                  datagroupByState.get(total[i]).push(fieldData[j]);
                }
            }

        }
   }

    let finaldataGroupByState=[];
    for(var l=0;l<total.length;l++){

      let localarray=datagroupByState.get(total[l]);
      let sumArea=0;
      let sumProdution=0;
      let area_max=[];
      let production_max=[];

      for(var k=0;k<localarray.length;k++){
        if(localarray[k].Area===null || localarray[k].Area===undefined){
          localarray[k].Area=0;
        }
        if(localarray[k].Production===null || localarray[k].Production===undefined){
          localarray[k].Production=0;
        }
        sumArea=sumArea+parseInt(localarray[k].Area);
        sumProdution=sumProdution+parseInt(localarray[k].Production);
        area_max.push(parseInt(localarray[k].Area));
        production_max.push(parseInt(localarray[k].Production));
      }

      let  avgArea=sumArea/localarray.length;
      let avgProd=sumProdution/localarray.length;
      let max_Area=Math.max(...area_max);
      let Max_production=Math.max(...production_max);
      let state=total[l];

      finaldataGroupByState.push(
        { [groupby]:state,
        'Avg_area':avgArea,
        'Avg_production':avgProd,
        'Max_area':max_Area,
        'Max_production':Max_production
        }
      );
    }
    return finaldataGroupByState;

}
generateTableData(data){
  //let data=this.state.fieldData;
  let groupBy=this.state.groupBy;
  let radioChecked=this.state.radioChecked;

  let totalStates=this.generateArray(data,'State_name');
  let totalDistrict=this.generateArray(data,'District_Name');
  let totalUniqueCrop=this.generateArray(data,'Crop');
  let totalUniqueSeason=this.generateArray(data,'Season');
  let totalUniqueCrop_Year=this.generateArray(data,'Crop_Year');

  let fieldData=data;

    if(radioChecked==="Average"){
        console.log("inside radiochecked=average");
        if(groupBy==="noGroup"){

            let tablecomp=new Map();
            let colsdata=this.state.cols;
            tablecomp.set('cols',colsdata);
            tablecomp.set('data',data);
            fieldData=tablecomp;

        }else if(groupBy==="State_name"){

            console.log("Inside else if of groupBY state Name");
            let finaldataGroupByState=this.groupBy(totalStates,fieldData,groupBy);
            delete finaldataGroupByState['Max_area'];
            delete finaldataGroupByState['Max_production'];
            let tablecomp=new Map();
            tablecomp.set('cols',[{ key: 'State_name', label:'State' },
            { key: 'Avg_area', label:'Average Area' },
            { key: 'Avg_production', label:'Average Production' }]);
            tablecomp.set('data',finaldataGroupByState);
            fieldData=tablecomp;

      }else if(groupBy==="District_Name"){

            console.log("inside District");
            let finaldataGroupByState=this.groupBy(totalDistrict,fieldData,groupBy);
            delete finaldataGroupByState['Max_area'];
            delete finaldataGroupByState['Max_production'];
            let tablecomp=new Map();
            tablecomp.set('cols',[{ key: 'District_Name', label:'District' },
            { key: 'Avg_area', label:'Average Area' },
            { key: 'Avg_production', label:'Average Production' }]);
            tablecomp.set('data',finaldataGroupByState);

            fieldData=tablecomp;

     }else if(groupBy==="Crop_Year"){

            let finaldataGroupByState=this.groupBy(totalUniqueCrop_Year,fieldData,groupBy);
            delete finaldataGroupByState['Max_area'];
            delete finaldataGroupByState['Max_production'];
            let tablecomp=new Map();
            tablecomp.set('cols',[{ key: 'Crop_Year', label:'Crop Year' },
            { key: 'Avg_area', label:'Average Area' },
            { key: 'Avg_production', label:'Average Production' }]);
            tablecomp.set('data',finaldataGroupByState);
            fieldData=tablecomp;

      }else if(groupBy==="Crop"){

            let finaldataGroupByState=this.groupBy(totalUniqueCrop,fieldData,groupBy);
            delete finaldataGroupByState['Max_area'];
            delete finaldataGroupByState['Max_production'];
            let tablecomp=new Map();
            tablecomp.set('cols',[{ key: 'Crop', label:'Crop' },
            { key: 'Avg_area', label:'Average Area' },
            { key: 'Avg_production', label:'Average Production' }]);
            tablecomp.set('data',finaldataGroupByState);
            fieldData=tablecomp;

     }else if(groupBy==="Season"){

            let finaldataGroupByState=this.groupBy(totalUniqueSeason,fieldData,groupBy);
            delete finaldataGroupByState['Max_area'];
            delete finaldataGroupByState['Max_production'];
            let tablecomp=new Map();
            tablecomp.set('cols',[{ key: 'Season', label:'Season' },
            { key: 'Avg_area', label:'Average Area' },
            { key: 'Avg_production', label:'Average Production' }]);
            tablecomp.set('data',finaldataGroupByState);
            fieldData=tablecomp;

     }

  }else if(radioChecked==="Max"){
    if(groupBy==="noGroup"){

        let tablecomp=new Map();
        let colsdata=this.state.cols;
        tablecomp.set('cols',colsdata);
        tablecomp.set('data',data);
        fieldData=tablecomp;

    }else if(groupBy==="State_name"){

        console.log("Inside else if of groupBY state Name");
        let finaldataGroupByState=this.groupBy(totalStates,fieldData,groupBy);
        delete finaldataGroupByState['Avg_area'];
        delete finaldataGroupByState['Avg_production'];
        let tablecomp=new Map();
        tablecomp.set('cols',[{ key: 'State_name', label:'State' },
        { key: 'Max_area', label:'Max Area' },
        { key: 'Max_production', label:'Max Production' }]);
        tablecomp.set('data',finaldataGroupByState);
        fieldData=tablecomp;

  }else if(groupBy==="District_Name"){

        console.log("inside District");
        let finaldataGroupByState=this.groupBy(totalDistrict,fieldData,groupBy);
        delete finaldataGroupByState['Avg_area'];
        delete finaldataGroupByState['Avg_production'];
        let tablecomp=new Map();
        tablecomp.set('cols',[{ key: 'District_Name', label:'District' },
        { key: 'Max_area', label:'Max Area' },
        { key: 'Max_production', label:'Max Production' }]);
        tablecomp.set('data',finaldataGroupByState);

        fieldData=tablecomp;

 }else if(groupBy==="Crop_Year"){

        let finaldataGroupByState=this.groupBy(totalUniqueCrop_Year,fieldData,groupBy);
        delete finaldataGroupByState['Avg_area'];
        delete finaldataGroupByState['Avg_production'];
        let tablecomp=new Map();
        tablecomp.set('cols',[{ key: 'Crop_Year', label:'Crop Year' },
        { key: 'Max_area', label:'Max Area' },
        { key: 'Max_production', label:'Max Production' }]);
        tablecomp.set('data',finaldataGroupByState);
        fieldData=tablecomp;

    }else if(groupBy==="Crop"){

        let finaldataGroupByState=this.groupBy(totalUniqueCrop,fieldData,groupBy);
        delete finaldataGroupByState['Avg_area'];
        delete finaldataGroupByState['Avg_production'];
        let tablecomp=new Map();
        tablecomp.set('cols',[{ key: 'Crop', label:'Crop' },
        { key: 'Max_area', label:'Max Area' },
        { key: 'Max_production', label:'Max Production' }]);
        tablecomp.set('data',finaldataGroupByState);
        fieldData=tablecomp;

   }else if(groupBy==="Season"){

        let finaldataGroupByState=this.groupBy(totalUniqueSeason,fieldData,groupBy);
        delete finaldataGroupByState['Avg_area'];
        delete finaldataGroupByState['Avg_production'];
        let tablecomp=new Map();
        tablecomp.set('cols',[{ key: 'Season', label:'Season' },
        { key: 'Max_area', label:'Max Area' },
        { key: 'Max_production', label:'Max Production' }]);
        tablecomp.set('data',finaldataGroupByState);
        fieldData=tablecomp;

    }

  }else if(radioChecked==="both"){
    if(groupBy==="noGroup"){

        let tablecomp=new Map();
        let colsdata=this.state.cols;
        tablecomp.set('cols',colsdata);
        tablecomp.set('data',data);
        fieldData=tablecomp;

    }else if(groupBy==="State_name"){

        console.log("Inside else if of groupBY state Name");
        let finaldataGroupByState=this.groupBy(totalStates,fieldData,groupBy);
        let tablecomp=new Map();
        tablecomp.set('cols',[{ key: 'State_name', label:'State' },
        { key: 'Avg_area', label:'Average Area' },
          { key: 'Max_area', label:'Max Area' },
        { key: 'Avg_production', label:'Average Production' },
          { key: 'Max_production', label:'Max Production' }]);
        tablecomp.set('data',finaldataGroupByState);
        fieldData=tablecomp;

  }else if(groupBy==="District_Name"){

        console.log("inside District");
        let finaldataGroupByState=this.groupBy(totalDistrict,fieldData,groupBy);
        let tablecomp=new Map();
        tablecomp.set('cols',[{ key: 'District_Name', label:'District' },
        { key: 'Avg_area', label:'Average Area' },
          { key: 'Max_area', label:'Max Area' },
        { key: 'Avg_production', label:'Average Production' },
          { key: 'Max_production', label:'Max Production' }]);
        tablecomp.set('data',finaldataGroupByState);

        fieldData=tablecomp;

 }else if(groupBy==="Crop_Year"){

        let finaldataGroupByState=this.groupBy(totalUniqueCrop_Year,fieldData,groupBy);
        let tablecomp=new Map();
        tablecomp.set('cols',[{ key: 'Crop_Year', label:'Crop Year' },
        { key: 'Avg_area', label:'Average Area' },
          { key: 'Max_area', label:'Max Area' },
        { key: 'Avg_production', label:'Average Production' },
          { key: 'Max_production', label:'Max Production' }]);
        tablecomp.set('data',finaldataGroupByState);
        fieldData=tablecomp;

    }else if(groupBy==="Crop"){

        let finaldataGroupByState=this.groupBy(totalUniqueCrop,fieldData,groupBy);
        let tablecomp=new Map();
        tablecomp.set('cols',[{ key: 'Crop', label:'Crop' },
        { key: 'Avg_area', label:'Average Area' },
          { key: 'Max_area', label:'Max Area' },
        { key: 'Avg_production', label:'Average Production' },
          { key: 'Max_production', label:'Max Production' }]);
        tablecomp.set('data',finaldataGroupByState);
        fieldData=tablecomp;

   }else if(groupBy==="Season"){

        let finaldataGroupByState=this.groupBy(totalUniqueSeason,fieldData,groupBy);
        let tablecomp=new Map();
        tablecomp.set('cols',[{ key: 'Season', label:'Season' },
        { key: 'Avg_area', label:'Average Area' },
          { key: 'Max_area', label:'Max Area' },
        { key: 'Avg_production', label:'Average Production' },
          { key: 'Max_production', label:'Max Production' }]);
        tablecomp.set('data',finaldataGroupByState);
        fieldData=tablecomp;

    }
  }else{
    let tablecomp=new Map();
    let colsdata=this.state.cols;
    tablecomp.set('cols',colsdata);
    tablecomp.set('data',data);
    fieldData=tablecomp;
  }

  return fieldData;
}
handleOptionChange(changeEvent) {
  console.log("radio button value",changeEvent.target.value);
  this.setState({
    radioChecked: changeEvent.target.value
  });
}
searhText(event){
  //console.log(event.target.value);
  this.setState({searchText:event.target.value,
        from:'',
        to:''
  });
}
searchBy(event){
  this.setState({searchBy:event.target.value});
}
range(event){
  console.log("event.id",event.target.id);
  if(event.target.id==="from"){
    let fromValue=event.target.value;
    this.setState({from:fromValue});
  }else{
    let toValue=event.target.value;
    this.setState({to:toValue});
  }
}
filterData(){
  let data=this.state.fieldData;
  let filteredData=[];
  let searchText=this.state.searchText;
  let searchBy=this.state.searchBy;

  //console.log(searchBy);
  if(searchText!==""){
    if(searchBy==="Crop_Year"){
      if(searchText.match(/[0-9]+[-][0-9]+/g)!==null){
          let searchArray=searchText.split("-");
          if(searchArray.length<=2){
            let from=parseInt(searchArray[0]);
            let to=parseInt(searchArray[1]);
            if(to!==-1 && to>=from){
              for(var index=0;index<data.length;index++){
                  console.log(data[index]);
                  if(data[index][searchBy]>=from && data[index][searchBy]<=to){
                    filteredData.push(data[index]);
                  }
              }
            }else{
              for(let index=0;index<data.length;index++){
                  if(data[index][searchBy].indexOf(from)!==-1){
                    filteredData.push(data[index]);
                  }
              }
            }
          }else{
            alert("Invalid Format: valid format are ex. 2005-3000");
          }
      }else{
        for(let index=0;index<data.length;index++){
          //  console.log(data[index]);
            if(data[index][searchBy].toLowerCase().indexOf(searchText.toLowerCase())!==-1){
              filteredData.push(data[index]);
            }
        }
      }

    }else{
      for(let index=0;index<data.length;index++){
        //  console.log(data[index]);
          if(data[index][searchBy].toLowerCase().indexOf(searchText.toLowerCase())!==-1){
            filteredData.push(data[index]);
          }
      }
    }

  }else if(this.state.from!=='' & this.state.to!==''){
    console.log("inside from/to else if");
    let from=parseInt(this.state.from);
    let to=parseInt(this.state.to);
    console.log("from",from);
    console.log("to",to);
      if(from<=to){
        for(var index=0;index<data.length;index++){
            console.log(data[index]);
            if(data[index][searchBy]>=from && data[index][searchBy]<=to){
              filteredData.push(data[index]);
            }
        }
      }else{
        filteredData=data;
      }
  }else{
    filteredData=data;
  }
  console.log(filteredData);
return filteredData;
}

  render() {
    console.log("inside Render");
    let filteredData=this.filterData();
    let tabledata=this.generateTableData(filteredData);
  let data=tabledata.get("data");
  let cols=tabledata.get("cols");
    return (
          <div>
          <div className="groupDiv">
            <h2>Crop Data</h2>
            <div className="groupDivContent">
          <label>Group By: </label>
          <select className="styled-select slate" onChange={this.groupBySelection.bind(this)}>
          <option value="noGroup">--No Grouping--</option>
          <option value="State_name">State</option>
          <option value="District_Name">District</option>
          <option value="Crop_Year">Crop Year</option>
          <option value="Season">Season</option>
          <option value="Crop">Crop</option>
          </select>
          <input type="radio" name="metrics"  onChange={this.handleOptionChange.bind(this)} checked={this.state.radioChecked==='Average'} value="Average"/>Average &nbsp;
          <input type="radio" name="metrics"  onChange={this.handleOptionChange.bind(this)} checked={this.state.radioChecked==='Max'} value="Max"/>Max &nbsp;
          <input type="radio" name="metrics"  onChange={this.handleOptionChange.bind(this)} checked={this.state.radioChecked==='both'} value="both"/>Both
          <br/>
          <br/>
          <div className="filterElementsDiv">
          {this.state.searchBy==="Area" || this.state.searchBy==="Production"?
                <div className="fromToDiv">
                    <label>From: </label>
                    <input className="text-input" type="text" value={this.state.from} id="from" onChange={this.range.bind(this)} placeholder="Range From"/>&nbsp;
                    <label>To: </label>
                    <input className="text-input" type="text" id="to" value={this.state.to} onChange={this.range.bind(this)} placeholder="Range To"/>&nbsp;
               </div>
          :
            <div>
                <label>Search: </label>
                <input className="text-input" type="text" onChange={this.searhText.bind(this)} placeholder="Search"/>&nbsp;
            </div>
        }
          <label>Search By: </label>
          <select className="styled-select slate" onChange={this.searchBy.bind(this)}>
          <option value="State_name">State</option>
          <option value="District_Name">District_Name</option>
          <option value="Crop_Year">Crop_Year</option>
          <option value="Season">Season</option>
          <option value="Crop">Crop</option>
          <option value="Area">Area</option>
          <option value="Production">Production</option>
          </select>
          </div>
          <div className="tableDiv"><TableComp cols={cols} data={data}/></div>
          </div>
          </div>
          </div>
    );
  }
}
export default DataDisplay;
